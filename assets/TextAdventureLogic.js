var state = "start"
var fullOutput = "";
var gameType = "";
//var selectedModel = none;

var inputBox = document.getElementById("inputBox");
var buttonEnter = document.getElementById("buttonEnter")
var outputRow = document.getElementById("outputRow");

buttonEnter.addEventListener("click", function(event){
    stateMahine();
});
document.addEventListener('keydown', function(event) {
    if(event.key == "Enter") stateMahine();
});

function stateMahine(){
    var userInput = inputBox.value;
    if(userInput == ""){
        console.log("No user Input");
        return;
    }
    switch(state){
        case "start":
            if (userInput.toUpperCase() == "START"){
                console.log("The game has started!");
                addToOutput(userInput, true);
                addToOutput("The game has started!");
                switchState("typeSelect");
                addToOutput("Select the type of story you want");
                displayAdventureOptions();
            }else{
                unknownCommand(userInput);
            }
            break;
        case "typeSelect":
            if(userInput.toUpperCase() == "1" || userInput.toUpperCase() == "ADVENTURE"){
                addToOutput(userInput, true);
                addToOutput("You chose an advetnure story!");
                gameType = "adventure";
                switchState("chooseStart");
                displayStartOption()
            }else if(userInput.toUpperCase() == "2" || userInput.toUpperCase() == "SCIENCE FICTION" || userInput.toUpperCase() == "SCI FI"){
                addToOutput(userInput, true);
                addToOutput("You chose a Sci-Fi story!");
                gameType = "scifi";
                switchState("chooseStart");
                displayStartOption()
            }else if(userInput.toUpperCase() == "3" || userInput.toUpperCase() == "WESTERN"){
                addToOutput(userInput, true);
                addToOutput("You chose a western story!");
                gameType = "western";
                switchState("chooseStart");
                displayStartOption()
            }else{
                unknownCommand(userInput);
                displayAdventureOptions();
            }
            break;
        case "chooseStart":
            if(userInput.toUpperCase() == "OWN"){
                addToOutput(userInput, true);
                addToOutput("You chose to write your own start, so start writting.");
                switchState("gameLoop");
            }else if(userInput.toUpperCase() == "MASTER"){
                addToOutput(userInput, true);
                addToOutput("You choose the Dungeon Master to start your story.");
                sendPostReq("", 100);
                switchState("gameLoop");
            }else{
                unknownCommand(userInput);
                displayStartOption();
            }
            break;
        case "gameLoop":
            if(userInput.toUpperCase() == "END" || userInput.toUpperCase() == "STOP" || userInput.toUpperCase() == "THE END"){
                addToOutput(userInput,true);
                addToOutput("The story is over!");
                switchState("end");
                displayEndOption();
            }else{
                addToOutput(userInput, true);
                addToOutput("Dungeon Master is thinking...");
                sendPostReq(userInput, 50); //this sends the req to the service, need to test if the connection works
            }
            break;
        case "end":
            if(userInput.toUpperCase() == "STORY"){
                //Izpiši obnovo celotne zgodbe
                addToOutput(userInput,true);
                addToOutput(fullOutput);
                displayEndOption();
            }else if(userInput.toUpperCase() == "DOWNLOAD"){
                //ustvari dokument, vpiši vsebino zgodbe in poberi
                addToOutput(userInput,true);
                var text = fullOutput;
                var fileName = "AIDungeonStory.txt"
                download(fileName, text);
            }else if(userInput.toUpperCase() == "RESTART"){
                addToOutput(userInput,true);
                switchState("start");
                //clear all text
                fullOutput = "";
                outputRow.innerHTML = '';
                addToOutput("This is a game where you can play through any adventure story you want to. The Dungeon Master will guide you through your story. Your job is to get to the end of the story, the one you want or not.");
                addToOutput("Write your inputs into the bottom box. Firstly there will be instructions to choosing the start of your adventure. Later there won't be any instructions, what you write is what happens in the story. Be careful what you write because the Dungeon Master will continue the story exactly how you wrote it.");
                addToOutput("To begin your adventure, write 'start' and follow the prompts.");
            }
            break;
    }
    // switch(state){
    //     case "start":
    //         if (userInput.toUpperCase() == "ZAČNI" || userInput.toUpperCase() == "ZACNI"){
    //             console.log("Igra začela!");
    //             addToOutput(userInput, true);
    //             addToOutput("Igra je začela!");
    //             switchState("typeSelect");
    //             addToOutput("Izberite eno od možnih tipov avantur");
    //             displayAdventureOptions();
    //         }else{
    //             unknownCommand(userInput);
    //         }
    //         break;
    //     case "typeSelect":
    //         if(userInput.toUpperCase() == "1" || userInput.toUpperCase() == "PUSTOLOVSKA"){
    //             addToOutput("Izbrali ste pustolovsko avanturo");
    //             switchState("gameLoop");
    //         }else if(userInput.toUpperCase() == "2" || userInput.toUpperCase() == "FANTAZIJA"){
    //             addToOutput("Izbrali ste fantazijsko avanturo");
    //             switchState("gameLoop");
    //         }else if(userInput.toUpperCase() == "3" || userInput.toUpperCase() == "ZNANSTVENA FANTASTIKA"){
    //             addToOutput("Izbrali ste znastveno fantastično avanturo");
    //             switchState("gameLoop");
    //         }else if(userInput.toUpperCase() == "4" || userInput.toUpperCase() == "VESTERN"){
    //             addToOutput("Izbrali ste vestern avaturo");
    //             switchState("gameLoop");
    //         }else{
    //             unknownCommand(userInput);
    //             displayAdventureOptions();
    //         }
    //         break;
    //     case "gameLoop":
    //         if(userInput.toUpperCase() == "KONČAJ" || userInput.toUpperCase() == "KONCAJ" || userInput.toUpperCase() == "KONEC"){
    //             addToOutput("Konec Zgodbe!");
    //             switchState("end");
    //             addToOutput("Če želiste videti obnovo vaše zgodbe, napišite 'obnova', če pa želiste prenesti datoteko z vašo zgodbo na napravo, našišite 'prenesi'");
    //             addToOutput("Napišite 'ponovno', če želiste začeti novo avanturo.");
    //             addToOutput("OPOZORILO! Stara zgodba se bo zgubila"); //Make this stand out
    //         }else{
    //             //Here we get the model and feed it the userInput and wait for the output, then we display it to the user, in the mean time, block further inputs
    //         }
    //         break;
    //     case "end":
    //         if(userInput.toUpperCase() == "OBNOVA"){
    //             //Izpiši obnovo celotne zgodbe
    //         }else if(userInput.toUpperCase() == "prenesi"){
    //             //ustvari dokument, vpiši vsebino zgodbe in poberi
    //         }else if(userInput.toUpperCase() == "PONOVNO"){
    //             switchState("start");
    //             //clear all text
    //             outputRow.innerHTML = '';
    //             addToOutput("To je igrica, kjer lahko igraste skozi kakršno koli avanturo, ki si jo želiste. Dungeon Master vam bom pomagal skozi zgodbo, vaš cilj pa je priti do konca zgodbe, tistega ki hočeste ali pa ne.");
    //             addToOutput("V spodnje okno vpišeste ukaze. Na začetku vam bodo v tem oknu prikazane možnosti, da začneste vašo avanturo. Kasneje pa nebo ukazov, lahko napišeste karkoli hočeste, da se zgodi v avanturi. Pišite čitljivo, saj Dungeon Master bo nadaljeval zgodbo točno tako, kot jo napišeste.");
    //             addToOutput("Da začneste igrati, napišite v spodnje oknce 'začni' in kliknite Enter");
    //         }
    //         break;
    // }
}

function download(file, text) { 
              
    //creating an invisible element 
    var element = document.createElement('a'); 
    element.setAttribute('href',  
    'data:text/plain;charset=utf-8, ' 
    + encodeURIComponent(text)); 
    element.setAttribute('download', file); 
  
    // Above code is equivalent to 
    // <a href="path of file" download="file name"> 
  
    document.body.appendChild(element); 
  
    //onClick property 
    element.click(); 
  
    document.body.removeChild(element); 
} 

function displayEndOption(){
    addToOutput("Write these commands into the box bellow:");
    addToOutput("story ---> Will display the full story that was generated");
    addToOutput("download ---> Download thw whole story in text formt to your device");
    addToOutput("restart ---> Start another adventure from the genre select state");
    addToOutput("WARNING! The old adventure will be lost"); //Make this stand out
}

function displayAdventureOptions(){
    // addToOutput("1 - Pustolovksa")
    // addToOutput("2 - Fantazija")
    // addToOutput("3 - Znanstvena Fantastika")
    // addToOutput("4 - Vestern")
    addToOutput("1 ---> Adventure")
    addToOutput("2 ---> Science Fiction")
    addToOutput("3 ---> Western")
}

function displayStartOption(){
    addToOutput("Write these commands into the box bellow:");
    addToOutput("own ---> Type your own start of the story, and the Dungeon Master will continue.");
    addToOutput("master ---> Let the Dungeon Master start the story by generating some starting text (WARNING - May not work properly).");
}

function switchState(newState) {
    state = newState;
}

function unknownCommand(userInput){
    // addToOutput("Ne razumem tega ukaza - " + userInput);
    addToOutput("Unknown command - " + userInput);
}

function addToOutput(userInput, byUser = false){
    var newCol = document.createElement("div");
    var newP = document.createElement("p");
    if(byUser==true){
        var newTextNode = document.createTextNode(">>"+userInput);
        newP.classList.add("grayText");
    }else{
        var newTextNode = document.createTextNode(userInput);
        newP.classList.add("whiteText");
    }
    
    newCol.classList.add("col-sm-12", "text-wrap");

    newP.appendChild(newTextNode);
    newCol.appendChild(newP);
    outputRow.appendChild(newCol);

    inputBox.value = "";
    outputRow.scrollTop = outputRow.scrollHeight - outputRow.clientHeight;
}

function sendPostReq(userText, generateLength){
    var readyOutput = "";
    if(fullOutput.length == 0){
        readyOutput = userText;
    }else{
        readyOutput = fullOutput + " " + userText;
    }
    console.log("LOG_READY_OUTPUT: " + readyOutput + " //LOG_READY_OUTPUT_LENGTH: " + readyOutput.split(/[ ,]+/).length);
    var fullLength = readyOutput.length;

    switch(gameType){
        case "adventure":
            console.log("Adventure generator");
            $.ajax({
                type: "POST",
                url: "https://gpt2-adventure-qdcqcmqqta-ew.a.run.app",
                // "headers": {"Access-Control-Allow-Methods": "DELETE, POST, GET, OPTIONS","Access-Control-Allow-Headers": "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With"},
                dataType: "json",
                data: JSON.stringify({length: (generateLength).toString(), temperature: "0.7", top_k: "40", prefix: readyOutput}),
                beforeSend: function (response) {
                    $('#buttonEnter').addClass("running");
                    $('#buttonEnter').prop("disabled", true);
                  },
                  success: function (response) {
                    $('#buttonEnter').removeClass("running");
                    $('#buttonEnter').prop("disabled", false);
                    //Trim the text, save it and display it on screen
                    console.log("//LOG_RESPONSE_TEXT: " + response.text + " //LOG_RESPONSE_LENGTH: " + response.text.split(/[ ,]+/).length);
                    var lastindex = response.text.lastIndexOf(".") - fullOutput.length;
                    var text = response.text.substr(fullOutput.length, lastindex + 2).trim();
                    console.log(text);
                    addToOutput(text);
                    //Add The full output to memory for later use
                    fullOutput = fullOutput + text;
                    console.log(fullOutput);
                },
            });
            break;
        case "scifi":
            console.log("Sci-Fi generator");
            $.ajax({
                type: "POST",
                url: "https://gpt2-scifi-qdcqcmqqta-ew.a.run.app",
                // "headers": {"Access-Control-Allow-Methods": "DELETE, POST, GET, OPTIONS","Access-Control-Allow-Headers": "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With"},
                dataType: "json",
                data: JSON.stringify({length: (50).toString(), temperature: "0.7", top_k: "40", prefix: readyOutput}),
                beforeSend: function (response) {
                    $('#buttonEnter').addClass("running");
                    $('#buttonEnter').prop("disabled", true);
                  },
                  success: function (response) {
                    $('#buttonEnter').removeClass("running");
                    $('#buttonEnter').prop("disabled", false);
                    //Trim the text, save it and display it on screen
                    console.log("//LOG_RESPONSE_TEXT: " + response.text + " //LOG_RESPONSE_LENGTH: " + response.text.split(/[ ,]+/).length);
                    var lastindex = response.text.lastIndexOf(".") - fullOutput.length;
                    var text = response.text.substr(fullOutput.length, lastindex + 2).trim();
                    console.log(text);
                    addToOutput(text);
                    //Add The full output to memory for later use
                    fullOutput = fullOutput + text;
                    console.log(fullOutput);
                },
            });
            break;
        case "western":
            console.log("Western generator");
            $.ajax({
                type: "POST",
                url: "https://gpt2-western-qdcqcmqqta-ew.a.run.app",
                // "headers": {"Access-Control-Allow-Methods": "DELETE, POST, GET, OPTIONS","Access-Control-Allow-Headers": "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With"},
                dataType: "json",
                data: JSON.stringify({length: (50).toString(), temperature: "0.7", top_k: "40", prefix: readyOutput}),
                beforeSend: function (response) {
                    $('#buttonEnter').addClass("running");
                    $('#buttonEnter').prop("disabled", true);
                  },
                  success: function (response) {
                    $('#buttonEnter').removeClass("running");
                    $('#buttonEnter').prop("disabled", false);
                    //Trim the text, save it and display it on screen
                    console.log("//LOG_RESPONSE_TEXT: " + response.text + " //LOG_RESPONSE_LENGTH: " + response.text.split(/[ ,]+/).length);
                    var lastindex = response.text.lastIndexOf(".") - fullOutput.length;
                    var text = response.text.substr(fullOutput.length, lastindex + 2).trim();
                    console.log(text);
                    addToOutput(text);
                    //Add The full output to memory for later use
                    fullOutput = fullOutput + text;
                    console.log(fullOutput);
                },
            });
            break;
    }
}
