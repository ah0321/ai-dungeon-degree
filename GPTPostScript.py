def GPTPost(argPrefix):
    import requests
    req = requests.post('https://gpt-qdcqcmqqta-ew.a.run.app', json={'length': 80,
                                                                     'temperature': 0.7,
                                                                     'prefix': argPrefix,
                                                                     'include_prefix': False})
    text = req.json()['text']
    return text

if __name__ == "__main__":
    x = GPTPost()
    print(x)

